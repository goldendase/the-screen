const { fromJS, Set, List, Map, Record } = require('immutable');
const getDb = require('./db');

const fs = require('fs');

const fogs = ['noun', 'adjective', 'verb', 'adverb'];
const entityTypes = ['world', 'site', 'area', 'tile', 'item', 'character', 'faction', 'other'];
const styles = ['dark', 'horror', 'military', 'city', 'eastern', 'epic', 'wilderness', 'animal',
  'liquid', 'condition', 'legal', 'religion', 'neutral', 'color'
];

const wordSchema = Map({
    // Where it was pulled from
    _source: null,
    // Word itself, in the singular
    word: null,
    // Plural form (if null, engine will pluralize according to normal english 
    // rules but if special case, will use this string )
    plural: null,
    // Subjective (but consistently named) tags describing the style
    // of the word. E.g. 'dark', 'horror', 'neutral', 'eastern', '
    style: Set(),
    fog: null,
    // Applicable genres (e.g.) 'sci-fi', 'modern', 'fantasy'
    genres: Set(),
    // Which entity type(s) is this word suitable for - only applicable to
    // nouns
    entity: Set()
});

const rawSchema = Map({
  '_id': null,
  '_source': null,
  '_fields': null,
  '_desc': null
});


async function getInterface() {
  const { query, exec } = await getDb();

  return {
    refresh: (data) => getInterface(data),
    importRaw: async (data) => exec(query().insert(data.toJS())),
    replaceRaw: async (data) => await exec(query()
      .filter(r.row('_id').eq(data.get('_id')))
      .update(data.toJS())),
    deleteRaw: async (data) => await exec(query()
      .filter(r.row('_id').eq(data.get('_id')))
      .delete())
  };
};

module.exports = getInterface;