const r = require('rethinkdb');

const DEFAULT_DB = 'rpg';
const DEFAULT_TABLE = '_raw_source';

const getConnection = ( host = 'localhost', port = 28015 ) => {
  return new Promise((resolve, reject) => {
    r.connect( { host, port }, function(err, conn) {
      if (err) {
        return reject(err);
      } else {
        return resolve(conn);
      }
    });
  });
};

function dbi(builtQuery, conn) {
  const i = {
    normalized: (table = 'normalized') => builtQuery.table(table),
    raw: (table = '_raw_source') => builtQuery.table(table),
    query: (table = DEFAULT_TABLE) => builtQuery.table(table),
    exec: (query) => new Promise((resolve, reject) => {
      return query.run(conn, (err, result) => {
        if (err) {
          return reject(err);
        } else {
          return resolve(result);
        }
      })
    })
  };
  return i;
}

module.exports = async (conn, db = DEFAULT_DB) => {
  try {
    const conn = await getConnection();
    const dbEntry = r.db(db);
    return dbi(dbEntry, conn);
  } catch(err) {
    console.error('Cound to connect to DB');
    process.exit(-1);
  }
  return 
};
