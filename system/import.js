const { fromJS, Map } = require('immutable');
const getInterface = require('./extractor');

const i = await getInterface();

const init = Map({
  _id: 'table_1-1A',
  _part: 0,
  _category: 'Locations',
  _name: 'Locations Overview',
  _source: '_TAD',
});


const data = {
Structure’s Description (1d100)
Adamantine Aerial Amphibious Ancient Arachnid Astrological Asymmetrical Bizarre Black Bleak Blue Bronze Buried Celestial Circuitous Circular Clay Coiled Collapsing Concealed Contaminated Convoluted Corroded Criminal Crimson Crooked Crude Crumbling Crystalline Curious Cursed Cyclopean Decaying Deceptive Decomposing Defiled
Structure (1d100)
Abbey of the Aerie of the Asylum of the Aviary of the Barracks of the Bastion of the Bazaar of the Bluffs of the Brewery of the Bridge of the Cairn of the Canyon of the Carnival of the Castle of the Cathedral of the Cellars of the Chapel of the Chapterhouse of the Church of the City of the Cliffs of the Cloister of the Cocoon of the Coliseum of the Contrivance of the Cottage of the Court of the Crags of the Craters of the Crypt of the Demi-plane of the Dens of the Dimension of the Domain of the Dome of the Dungeons of the
Feature - first word (1d100)
Ant-
Ape- Baboon- Bat- Beetle- Bitter Blood Bone- Brain Broken Bronze Burned Cabalistic Carnal Caterpillar- Centipede- Changing Chaos- Cloud- Cockroach- Crimson Crippled Crocodile- Dark Death- Decayed Deceitful Deluded Dinosaur- Diseased Dragonfly- Dread Elemental Elephant- Feathered Fiery
Feature - second word (1d100)
Abbot Actor Alchemist Altar Apparition Apprentice Assassin Beast Behemoth Binder Bishop Breeder Brood Brotherhood Burrower Caller Captive Ceremony Chalice Changeling Chanter Circlet Clan Collector Combiner Congregation Coronet Crafter Crawler Creator Creature Crown Cult Cultists Daughter Demon
}

await i.importRaw(fromJS(data).merge(init));