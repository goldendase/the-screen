## vagabond

- once every 24 hours gets to reroll a skill check  choosing to keep the result

- +1 free level in non-combat focus at creation related to background

- on level up gain +1 skill point for non-combat / non-magic skill

start hp: 1d6 + con mod (min 1)
attack bonus: 1/2 level rounded down (0 at level 1)

## mage

- can learn magic

- choose any two spells as bonus (can pick one twice for a +1)

- concentration: using spells saps concentration as per spell. max concentration = 1+(highest spell level)+max(wis mod, con mod)
  * it's possible to cast a spell that would bring concentration below zero. when bringing concentration below zero roll (2d6 + negative concentration + level) and apply exhaustion effect as per magic exhaustion table

start hp: 1d6 + con mod (min 1)
attack bonus: 1/2 level rounded down (0 at level 1)

## warrior

- +1 free level in combat-related focus related to background

- once every 24 hours gets to negate a successful attack against them or force a missed attack to hit

- +2 hp every level up

start hp: 1d6 + 2 + con mod (min 1)
attack bonus: +1/level (1 at level 1)

## wild mage

- can use magic

- Starts with three spells, but all spells randomly chosen

- cannot learn spells from grimoires or other mages. on each level up, gains two random spells, with one guaranteed to be of the new level

- Every time the wild mage casts a spell, roll 1d10.
  1 - misfire - energy swells within the wild mage - take [caster level] dmg but restore +1 concentration
  2 - 9 - spell works as normal
  10 - cast spell with rank +1

start hp: 1d6 + con mod (min 1)
attack bonus: 1/2 level rounded down (0 at level 1)

## true believer

- can use divine magic

- spells don't cost concentration as they are the channeled power of the deity, but have associated negative consequences for the caster

- starts with two spells



