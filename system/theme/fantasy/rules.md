## saves

physical
--
(poison, endurance, disease, exhaustion)

evasion
--
dodging and diving

will
--
resist mental control, resist horror

saving throw scores start at 15
(decrease by -1 on each level up)

at each level:
physical = 15 - level + 1 + max(str, con)
evasion = 15 - level + 1 + max(dex, int)
mental = 15 - level + 1 + max(cha, str)

to make a save:
roll 1d20 >= save value == success


## skill checks

to make a skill check 
roll 2d6 + skill level + stat mod

if character does not even have level-0 in skill, sugger -1 penalty
(in certain cases, may not be able to perform it at tall)

difficulties should range 6 - 14+ (10 is even expert level might fail at this)

skill check may be modified up to +-2 depending on circumstance (e.g. lack of tools or hazardous conditions)

-- assistance
other pcs may assist in the effort. the pc rolls applicable skill check, and if successful original pc attempting the roll gets a +1 (max of +1 even if multiple assist)

-- opposed checks
roll same check for both, tie goes to PC



