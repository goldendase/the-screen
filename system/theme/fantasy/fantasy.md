## stats
str - physical prowess, melee combat, carryigng gear, grute force
dex - speed, evasion, manual dexterity, reaction time, combat initiative
con - enduring injury, toxins, going without food or sleep
int - memory, reasoning, technical skills, general education
wis - noticing things, making judgements, decreased chance of affliction on horror, reading situations, intuition
cha - command, charm, disguise (in the idenitity sense)

## character generation

two methods
--
roll 3d6 x 6
assign results in order
optionally, then set one to 14

or

assign 14, 12, 11, 10, 9, 7

attrib score
3      -2
4-7    -1
8-13    0
14-17  +1
18     +2

saves (default 0, roll 1d20 vs target)
--
poison / disease
will
reflex
magic
elements

initial hp: roll HD from race + con mod
hp per level: prior level hp + roll HD from race + con mod

max concentration: wis mod + highest level of known spell

## choose gender

male / female

## choose race

sizes are (tiny, small, normal, large, giant) with -1 to-hit large->small, +1 to-hit small->large. each size can only wield / use items of +- its own size (so a small can wield a normal weapon, but not a large one)

dwarf
min con: 10
str + 2
con + 1
cha - 1
HD: 1d8

size (small), darkvision, +1 all saves, 2 background choices (physical)

elf
min wisdom: 10
wis + 1
cha + 1
str - 1
HD: 1d6

size (normal), +1 magic save, apply any 1 background choice twice

halfling
min int: 10
dex + 2
int + 1
str - 2
HD: 1d4

size (small), +3 will save, 2 background choices (one physical, one mental)

human
+1 to stat of choice
no min
HD: 1d6

size (normal), 2 any background choices

## Choose background(s)

each background grants one free specific skill level
next you have two options:
1.) pick two skills from the learning table for that class background (can pick same skill twice)
2.) make three random rolls divided between the two tables (pick table first)

## Knowledge Skills

no character may start with skill level > 2

-- Every skill is leveled 1-8
Not having a skill means you can attempt it (at GM's discretion), but against the next highest DC for that check (so, if DC is 10, then 15, if it's 15 then 20, and so on). Stat mod still applies (if you're extremely intelligent, that will still help you analyze some financial records, even if you've never done it before).

for all skills marked with a 'crafting' tag, allows crafting of basic tier items within that domain at level 2, allows crafting of increased tier items every other level (fine at 4, exotic at 6, epic at 8)

Using a skill is
roll 1d20 + skill level + applicable stat mod

### Physical Skills
any skill with mod (str, con, dex) is physical

knowledge (metalworking) - craft metal devices, blacksmithing, ability to identify metals and their properties, appraisal of non-magical metal items
applied mod: str

knowledge (mechanical) - locksmithing, construct mechanical devices, understand and use mechanical devices, assess properties of mechanical creatures
applied mod: dex

knowledge (construction) - analyzing structures for abnormalities, construct makeshift defenses (given time and resources), identify wood and stone and their properties, fletching
mod: con

knowledge (wilderness) - identify edible and poisonous plans, track, determine orientation outdoors, assess properties of wild creatures
mod: con

feat of strength - climb a steep cliff, swim a rapid river, lift very heavy objects, hold an enemy down
mod: str

feat of dexterity - walk a tightrope, jump a wide chasm, fit into a very tight (but still plausible) place
mod: dex

stay hidden - hide in shadows to avoid detection, sneak past an enemy, maintain an effective disguise
mod: dex

### Mental Skills
any skill with mod (int, wis, cha) is mental

knowledge (biology) - apply basic field medicine, determine cause of death, assess properties of biological creatures
mod: wis

knowledge (arcane) - understand magical principles, identify types of magic and possible effects, interpret and use magical scrolls and items, assess properties of magical creatures
mod: int

knowledge (religion) - knowledge of the pantheon and related rituals necessary to bring about the blessings of the gods. effectiveness at communicating with dieties and their representatives. ability to interpret religious texts. 

knowledge (economics) - analyze financial records for discrepancies, manage an estate or faction, appraise value of an identified item
mod: int

awareness: - notice out of place objects in the environment when searching, anticipate ambushes or surprises
mod: int

knowledge (bardic) - general knowledge of history, folklore, politics. also any effectiveness of any performance of any expression of these.
mod: cha

knowledge (character) - understanding of motivations, sensing motivation in others, effective lying to others, ability to impersonate others
mod: cha

## Fighting style

Melee Weapon Training <Pick Weapon Class>
Attack Bonus = 1d20 + Skill level with weapon + size modifier + dex mod
Damage Bonus = Weapon damage roll + str mod

Ranged Weapon Training <Pick Weapon Class>
Attack Bonus = 1d20 + Skill level with weapon + size modifier + dex mod
Damage Bonus = Weapon damage roll + ammo damage bonus (if any)











-------- ideas ---------
## Alignment
Order -> Chaos
0 - 100
failing sanity check increases chaos, see order / chaos effects (each has its own benefits and drawbacks)
roll to start at random between 45 and 55 (certain races are +- in either direction on gen, halflings are always 50)

## Reputation
0 -> 100
0 - loathed / kill on sight
100 - fanatical devotion, as to a cult leader

## bodily damage

All player races are humanoids. 
Body parts: head, left arm, right arm, torso, left leg, right leg, left foot, right foot

Bleeding, past a certain threshold needs bandaging or you lose 1hp per hr

Can target limbs (math for weapon + attack + other mods)
limbs compose total hp 





