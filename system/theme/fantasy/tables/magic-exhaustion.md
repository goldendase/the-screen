2d6 + resulting concentration + level

12 - no ill effect
10-11 - spell succeeds, but take 1d3 damage
6-9 - spell backfires, apply 'exhausted' status
 (-1 to all rolls until rested for 8 hours)
5 - spell backfires, take [caster level]d6 damage
4 - summon random non-humanoid entity (not controlled by caster)
3 - spell succeeds, killing the caster
2 - Random spell with random targets is cast

