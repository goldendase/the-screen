physical means str con dex
mental means int wis cha

# barbarian
grew up in the heart of darkness

free skill: knowledge (wilderness)

growth table
1  +1 any stat
2  +2 any physical
3  +2 any physical
4  +2 any mental
5  feat of strength
6  awareness

learning table
1  any fighting style
2  knowledge (character)
3  feat of strength
4  any fighting style
5  awareness
6  feat of dexterity
7  knowledge (metalworking)
8  knowledge (wilderness)

# clergy
consecrated priest

free skill: knowledge (religion)

growth table
1  +1 any stat
2  +2 any mental
3  +2 any physical
4  +2 any mental
5  knowledge (biology)
6  knowledge (bardic)

learning table
1  knowledge (economics)
2  knowledge (character)
3  knowledge (biology)
4  knowledge (arcane)
5  awareness
6  knowledge (character)
7  knowledge (bardic) 
8  knowledge (religion)

# criminal
thief, murderer, forger, probably a liar, too

free skill: stay hidden

growth table
1  +1 any stat
2  +2 any mental
3  +2 any physical
4  +2 any mental
5  feat of dexterity
6  knowledge (mechanical)

learning table
1  awareness
2  knowledge (economics)
3  knowledge (bardic)
4  any combat
5  feat of dexterity
6  stay hidden
7  knowledge (character)
8  knowledge (mechanical)

# merchant
knowledge equals profit

free skill: knowledge (economics)

growth table
1  +1 any stat
2  +2 any mental
3  +2 any mental
4  +2 any mental
5  knowledge (construction)
6  knowledge (character)

learning table
1  awareness
2  any combat
3  find contact
4  construction
5  knowledge (choice)
6  awareness
7  trade
8  persuade

# noble
there are two kinds

free skill: lead-0
recommended skills (lead, find contact, administer)

growth table
1  +1 any stat
2  +2 any mental
3  +2 any mental
4  +2 any mental
5  find contact
6  any skill

learning table
1  administer
2  any combat
3  find contact
4  knowledge (choice)
5  lead
6  awareness
7  knowledge (politics)
8  persuade

# scholar
into books

free skill: knowledge (choice)

growth table
1  +1 any stat
2  +2 any mental
3  +2 any mental
4  +2 any mental
5  find contact
6  any skill

learning table
1  administer
2  any skill
3  find contact
4  mechanical
5  persuade
6  awareness
7  knowledge (choice)
8  persuade

# healer
does his best

free skill: healing-0
recommended skills (administer, persuade, find contact)

growth table
1  +1 any stat
2  +2 any physical
3  +2 any mental
4  +2 any mental
5  find contact
6  any skill

learning table
1  administer
2  find contact
3  mechanical
4  awareness
5  healing
6  persuade
7  trade
8  administer

# entertainer
there's no business quite like it

free skill: perform-0
recommended skills (perform, persuade, find contact)

growth table
1  +1 any stat
2  +2 any mental
3  +2 any mental
4  +2 any physical
5  find contact
6  any skill

learning table
1  any combat
2  find contact
3  mechanical
4  awareness
5  perform
6  perform
7  exert
8  persuade

# soldier

free skill: any combat-0

growth table
1  +1 any stat
2  +2 any physical
3  +2 any physical
4  +2 any physical
5  exert
6  any skill

learning table
1  administer
2  any combat
3  exert
4  mechanical
5  lead
6  awareness
7  sneak
8  survival

# Commoner
Peasant or lower-class town-deller

free skill: exert-0

growth table
1  +1 any stat
2  +2 any physical
3  +2 any physical
4  +2 any physical
5  exert
6  any skill

learning table
1  find contact
2  exert
3  mechanical
4  sneak
5  survival
6  trade
7  awareness
8  knowledge (mundane)