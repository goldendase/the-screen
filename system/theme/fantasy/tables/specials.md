## specials

alert
-- type: mundane
level 1: +1 skill in awareness. roll initiative twice, take best result.
level 2: always act first in combat (unless others have alert, then initiative order)

favored weapon
-- type: combat
level 1: get +1 to their attack and damage rolls in a specific weapon type (sub-class of parent weapon skills - so e.g. longsword or warhammer not bladed or blunt), attackers using same weapon type against you get -1 to their attack roll
level 2: when using favored weapon, can choose to roll attack twice (and keep better result)

serene mind
-- type: magic
level 1: once every 24 hours, cast a spell of level (1/2 * highest spell level, rounded up) for free.
level 2: when casting a spell that would bring your concentration to 0 (or below), roll 1d6. on a 6, that spell is free
