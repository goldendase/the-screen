import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './app/App';
import rootReducer from './app/reducers';
import { createCampaign } from './app/campaign-client';

const store = createStore(rootReducer)
createCampaign((hash) => store.dispatch({ type: 'CAMPAIGN_UPDATE', hash }));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, 
document.getElementById('app'));

document.addEventListener('keydown', (event) => {
  store.dispatch({ type: 'USER_KEYPRESS', key: event.key });
});


