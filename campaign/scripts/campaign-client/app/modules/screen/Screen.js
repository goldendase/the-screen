import React from 'react';
import { connect } from 'react-redux';
import moduleMap from '../module-map';

class Screen extends React.Component {
  constructor(props) {
    super(props);
    this.screenId = props.screenId;
    props.initScreen(props.screenId);
  }

  render() {
    const ScreenType = moduleMap(this.props.screenType);
    return (
      <div 
        onClick={() => this.props.focusScreen(this.screenId)}
        style={{ height: '100%' }}
      >
        <ScreenType screenId={this.props.screenId} />
      </div>);
  }
};

function mapStateToProps(state, props) {
  return {
    screenType: state.getIn(['screen_type', props.screenId])
  };
}

const mapDispatchToProps = dispatch => {
  return {
    focusScreen: (screenId) => dispatch({ type: 'FOCUS_SCREEN', screen_id: screenId }),
    initScreen: (screenId) => dispatch({ type: 'INIT_SCREEN', screen_id: screenId })
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Screen);