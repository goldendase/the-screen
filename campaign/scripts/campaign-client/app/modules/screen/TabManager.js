import React from 'react';
import { connect } from 'react-redux';

import moduleMap from '../module-map';

const getTabs = (frameIds, setActiveFrame) => frameIds.map((frameId, i) => 
  <li><button onClick={() => setActiveFrame(frameId)} classNames='button-clear tab'>
    Terminal {i}
  </button></li>
);

class TabManager extends React.Component {
  render() {
    const ScreenType = moduleMap(this.props.screenType);
    return (
      <div className='tab-manager'>
        <ul className='tab-container'>
          {getTabs(this.props.frameIds, this.props.setActiveFrame)}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isActiveFrame: (frameId) => state.get('active_frame') === frameId,
    frameIds: state.get('frames').keySeq()
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setActiveFrame: (frameId) => dispatch({ id: 'SET_ACTIVE_FRAME', frameId })
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TabManager);