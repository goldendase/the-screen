const { Map } = require('immutable');
import React from 'react';
import { connect } from 'react-redux';
import StatBlock from './StatBlock';
import { getCampaign } from '../../campaign-client';

class CharacterEditor extends React.Component {
  componentDidMount() {
    if (!this.props.generatorId) {
      const generatorId = getCampaign().createGenerator('character');
      this.props.setGeneratorId(generatorId);
    }
  }

  getMethodSelectControls(generator) {
    const chooseRollStat = () => generator.setGenTypeRoll();
    const chooseAssignStat = () => generator.setGenTypeAssign();

    return generator.hasStatGenType() ?
      null :
      <div>
        <div className='row'>
          <div className='column'>Choose Generation Type</div>
        </div>
        <div className='row'>
          <div className='column'>
            <button className="button-clear" onClick={chooseRollStat}>Roll</button>
          </div>
          <div className='column'>
            <button className="button-clear" onClick={chooseAssignStat}>Assign</button>
          </div>
          <div className='column'></div>
        </div>
      </div>;
  }

  getStatBlock(character, generator) {
    return (
      <div>
        <div className='row'>
          <div className='column block-header'>
            <h6>Stats</h6>
          </div>
        </div>
        <div className='row'>
          <div className='column'>
            {character.getStats().map((value, stat) => <StatBlock
              key={stat}
              statName={stat}
              statVal={value}
              showAssignStat={this.shouldShowSetButton(generator)}
              onAssignClicked={() => generator.apply14Set(stat)}
            />).toList()}
          </div>
        </div>
      </div>
    );
  }

  getAssignStatControls(generator) {
    if (!generator.hasStatGenType()) {
      return null;
    }

    const nextStat = generator.getNextUnassignedStat();

    return <div>
      <div className='row'>
        <div className='column' style={{ textAlign: 'center' }}>
          Choose result for {nextStat}:
        </div>
      </div>
      <div className='row'>
        {generator.getStatPool().map((val, i) => 
          <div className='column' key={i}><button
            className='button-clear'
            onClick={() => generator.applyStatFromPool(val, nextStat)}
          >
            {val}
          </button></div>)}
      </div>
    </div>;
  }

  getNameBlock(character, generator) {
    return <div className='row'>
      <div className='column'>
        <button className='button-clear' onClick={() => generator.setFirstName()}>{character.getFirstName() || '<Generate>'}</button>
      </div>
      <div className='column'>
        <button className='button-clear' onClick={() => generator.setLastName()}>{character.getLastName() || '<Generate>'}</button>
      </div>
    </div>;
  }

  shouldShowSetButton(generator) {
    return (generator.isGenTypeRoll() && !generator.hasApplied14Set());
  }

  render() {
    if (!this.props.generatorId) {
      return <div>Loading...</div>
    }
    const generator = this.props.getGenerator();
    if (!generator.hasEntity()) {
      return <div style={{ align: 'center' }}>
        <button onClick={generator.createEntity}>Generate New Character</button>
      </div>
    }

    const character = generator.getEntity();
    
    return (
      <div className='character-edit-container'>
        <h5>Character Editor</h5>
        {this.getNameBlock(character, generator)}
        {this.getStatBlock(character, generator)}
        {this.getAssignStatControls(generator)}
        {this.getMethodSelectControls(generator)}
        <div className='character-edit-control-panel'>
          <div className='row'>
            <div className='column-offset-80 column-10'>
              <button className='button-clear' onClick={() => generator.destroyEntity()}>Discard</button>
            </div>
            <div className='column-10'>
            </div>
          </div>
        </div>
      </div>);
  }
};

function mapStateToProps(state, props) {
  const charGenState = state.getIn(['active_frame', props.screenId], Map());
  return {
    getGenerator: () => getCampaign().getRecord(charGenState.get('generator_id')),
    generatorId: charGenState.get('generator_id'),
    hash: state.get('campaign_hash')
  };
}

const mapDispatchToProps = dispatch => {
  return {
    setGeneratorId: (id, source) => dispatch({ type: 'SET_GENERATOR_ID', id, source }),
  };
};

CharacterEditor.defaultProps = {
  workInProgressChar: null,
  characterId: null,
  type: 'db'
};

export default connect(mapStateToProps, mapDispatchToProps)(CharacterEditor);