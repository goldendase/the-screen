import React from 'react';

export default class StatBlock extends React.Component {
  render() {
      const assignButton = this.props.showSetButton ?
        <button onClick={this.props.onAssignClicked}>{this.props.assignText}</button> :
        null;

      return (
        <div className='row stat-block'>
          <div className='column stat-block-name'><span>{this.props.statName}</span></div>
          <div className='column stat-block-value'><span>{this.props.statVal || ''}</span></div>
          <div className='column stat-block-button'>{assignButton}</div>
        </div>
      );
  }
};
