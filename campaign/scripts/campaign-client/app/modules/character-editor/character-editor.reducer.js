const { Map } = require('immutable');

const initialState = Map({
  generator_id: null
});

export default (state = initialState, action) => {
  if (action.type === 'INIT') {
    return Map().merge(initialState)
      .set('_id', action._id)
      .set('_reducer_type', action._reducer_type);
  }
  if (action.type === 'SET_GENERATOR_ID') {
    return state.set('generator_id', action.id);
  }
  return state;
};
