const { Map } = require('immutable');

const initialState = Map({
});

export default (state, action) => {
  if (action.type === 'INIT') {
    return Map().merge(initialState).set('_id', action._id)
      .set('_reducer_type', action._reducer_type);
  }
  return state;
};
