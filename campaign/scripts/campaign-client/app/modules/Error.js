import React from 'react';
import { connect } from 'react-redux';

class Error extends React.Component {
  render() {
    return <div>[ID: {this.props.screenId}] Unknown screen type {this.props.screenType}!</div>
  }
}

function mapStateToProps(state, props) {
  const screenType = state.get(['screen_type', props.screenId]);
  return {
    screenType
  };
}

export default connect(mapStateToProps)(Error);