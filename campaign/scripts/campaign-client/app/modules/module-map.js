import CharacterEditor from './character-editor/CharacterEditor';
import ViewError from './Error';

export default (type) => {
  const types = {
    'character-editor': CharacterEditor
  };
  if (types.hasOwnProperty(type)) {
    return types[type];
  }
  return ViewError;
};
