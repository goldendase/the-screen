import { OrderedMap, Map } from 'immutable';

const appState = Map({
  frames: OrderedMap(),
  active_frame: null,
});

export default function manageScreen(state = appState, action) {
  if (action.type === 'CAMPAIGN_UPDATE') {
    return state.set('campaign_hash', action.hash);
  }
  if (action.type === 'USER_KEYPRESS' && action.key === 'Meta') {
    return state;
  }
  if (action.type === 'SET_ACTIVE_FRAME') {
    return state.set('active_frame', action.frameId);
  }
  if (action.type === 'UPDATE_FRAME') {
    return state.setIn(['active_frame', action.frameId], action.frameData);
  }
  if (action.type === 'DELETE_FRAME') {
    return state.removeIn(['active_frame', action.frameId]);
  }
  return state;
}
