import React from 'react';
import { connect } from 'react-redux';
import TabManager from './modules/screen/TabManager.js';
import classes from '../css/main.css';

import getStyle from './styles';

const buildNav = () => <ul className='nav-container'>
  <li className='nav-item'>Create</li>
  <li className='nav-item'>Find</li>
  <li className='nav-item'>Run Game</li>
  <li className='nav-item fill'></li>
  <li className='nav-item'>Options</li>
</ul>;

class App extends React.Component {
  render() {
    return (
      <div style={getStyle('app')} className='graph-paper'>
        {buildNav()}
        <TabManager />
      </div>
    );
  }
};

export default connect(mapStateToProps)(App);
