const { create } = require('../../campaign-manager');

let campaign = null;

export const createCampaign = (hook) => {
  campaign = create(hook);
}

export const getCampaign = () => campaign;
