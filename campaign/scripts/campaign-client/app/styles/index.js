import { fromJS, Map } from 'immutable';

const palette = {
  primary: '#4D5A71',
  highlight: '#5D6798',
  contrast: '#CFA460',
  complement: '#403930',
  'complement-highlight': '#1C150B'
};

const colors = {
  bg: palette.primary,
  fg: '#F2F0ED',
  contrast: '#A05441',
  complA: '#7BB5D1',
  complB: '#648782',
};

const fonts = fromJS({
  volk: { fontFamily: '"Volkorn", serif' },
  aleg: { fontFamily: '"Alegreya", serif' },
  fell: { fontFamily: '"IM Fell DW Pica", serif' },
  aref: { fontFamily: '"Aref Ruqaa", serif' },
  vesper: { fontFamily: '"Vesper Libre", serif' }
});

const defaults = {
  font: 'aleg'
};

const defaultStyles = fromJS({
  app: {
    font: fonts.get(defaults.font),
    color: colors.fg,
    height: '100%'
  },
  screen: {
    overflow: 'hidden'
  }
});

const extendedStyles = fromJS({
  'active-screen': defaultStyles
    .get('screen')
    .set('border', `1px solid ${colors.fg}`),
  'system-text': {
    color: 'black'
  }
});

const navStyles = fromJS({
  'nav-container': {
    backgroundColor: colors.bg,
    display: 'flex',
  },
  'nav-item': {
    width: '10em',
    textAlign: 'center'
  }
});

const consolidatedStyles = defaultStyles
  .merge(extendedStyles)
  .merge(navStyles);

let defaultStyleCache = Map();
const getStyle = (alias) => {
  if (defaultStyleCache.has(alias)) {
    return defaultStyleCache.get(alias);
  }

  defaultStyleCache = defaultStyleCache
    .set(alias, consolidatedStyles.get(alias).toJS());

  return defaultStyleCache.get(alias);
};

export default getStyle;
