const getInitialState = require('./interfaces/common/initial-state');
const createDatabase = require('./core/db');

const throttledHook = (hook) => {
  let signalPending = false;
  return (hashCode) => {
    if (!signalPending) {
      signalPending = true;
      setTimeout(() => {
        hook(hashCode);
        signalPending = false;
      }, 0);
    }
  }
};

const create = (hook = () => {}, isThrottled = true) => {
  const campaignDatabase = createDatabase({
    hook: isThrottled ? throttledHook(hook) : hook
  });

  const state = getInitialState('campaign')
    .set('_timer_id', campaignDatabase.createRecord(getInitialState('timer')))
    .set('_events_id', campaignDatabase.createRecord(getInitialState('events')));

  const campaignId = campaignDatabase.createRecord(state);
  return campaignDatabase.getRecord(campaignId);
}

const load = (loadState, hook, isThrottled = true) => {
  const campaignDatabase = createDatabase({
    loadState,
    hook: isThrottled ? throttledHook(hook) : hook
  });

  return () => campaignDatabase.getRecordsByType('campaign').first();
}

module.exports = {
  create,
  load
};
