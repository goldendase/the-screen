const assert = require('chai').assert;

const campaign = require('../');
const { generateChar } = require('../generators/entities/character');

describe('campaign', () => {
  describe('character state', () => {
    const char = generateChar().finalize().getDbItem();

    it('should add a generated character into the campaign', () => {
      char._dbi().saveDb();
      const charInDb = () => campaign.findDbObjectById(char._dbi().getDbId());
      assert.exists(charInDb(), 'char exists in db');
      assert.equal(charInDb().getFirstName(), char.getFirstName(), 'char exists in db');
      char._setFirstName('foo')._dbi().saveDb();
      assert.exists(charInDb(), 'char exists in db');
      assert.equal(charInDb().getFirstName(), 'foo', 'name change propagates to DB');
    });
  })
});
