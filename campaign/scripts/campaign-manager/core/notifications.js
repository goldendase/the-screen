const initialState = require('../interfaces/common/initial-state');

function notification(state = initialState('notification'), dbi) {
  const n = {
    markSeen: () => dbi(state.set('status', 'seen')).save(),
    wasSeen: () => state.get('status') === 'seen',
    getName: () => state.get('name'),
    displayText: () => state.get('display_text')
  };
  return n;
}

module.exports = notification;
