module.exports = {
  notNullIn: (state, keyPath) => Array.isArray(keyPath) ?
    state.getIn(keyPath) !== null :
    state.get(keyPath) !== null,
  inRangeInc: (v, s, e) => v >= s && v <= e,
  timeToTurns: (inc, units) => {
    if (!units) {
      return 1;
    }
    if (/months?/.test(units)) {
      return _MONTH * inc;
    }
    if (/weeks?/.test(units)) {
      return _WEEK * inc;
    }
    if (/days?/.test(units)) {
      return _DAY * inc;
    }
    if (/hours?/.test(units)) {
      return _HOUR * inc;
    }
    if (/minutes?/.test(units)) {
      return Math.ceil(inc / 10);
    }
    return 1;
  }
};
