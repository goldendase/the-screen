const { v4 } = require('node-uuid');
const { List, Map, Set } = require('immutable');
const getInterface = require('../interfaces');
const hri = require('human-readable-ids').hri;

const generatePrettyId = (type) => `${type}-${hri.random()}`;

const trackInstances = (state = Map()) => {
  return {
    addInstance: (recordId, instanceId) => trackInstances(
      state.update(recordId, List(), (insList) => insList.push(instanceId))
    ),
    removeInstance: (recordId, instanceId) => trackInstances(
      state.update(recordId, List(), (insList) => insList.remove(instanceId))
    ),
    getInstances: (recordId) => state.get(recordId)
  };
};

const trackRecords = (state = Map()) => {
  return {
    addRecordByType: (type, id) => trackRecords(state
      .updateIn([type, id], List(), (l) => l.push(id))),
    removeRecordByType: (type, id) => trackRecords(state
      .update(type, (l) => l.remove(id))),
    getRecordsByType: (type) => state.get(type)
  };
};

const entityDbInterface = (dbi) => (state) => {
  const i = {
    state: () => i.isInstance() ? dbi._getInstanceRaw(i.getInstanceId()) : dbi._getRecordRaw(i.getRecordId()),
    getHashCode: () => state.hashCode(),
    getRecordId: () => state.get('_record_id'),
    isInstance: () => state.get('_instance_id', null) !== null,
    getInstanceId: () => state.get('_instance_id'),
    getPrettyId: () => state.get('_pretty_id'),
    getType: () => state.get('_type'),
    delete: () => i.isInstance(state) ? i.deleteInstance(state) : i.deleteRecord(state),
    deleteRecord: () => {
      if (i.isInstance(state)) {
        throw new Error(`Attempted to delete record from an instance type=${i.getType(state)} id=${i.getInstanceId(state)}`);
      }
      dbi.untrackRecord(i.getType(state), i.getRecordId(state));
      return dbi.deleteRecord(i.getRecordId(state));
    },
    deleteInstance: () => {
      if (!i.isInstance(state)) {
        throw new Error(`Attempted to delete instance from a record type=${i.getType(state)} id=${i.getRecordId(state)}`);
      }
      dbi.untrackInstance(i.getRecordId(state), i.getInstanceId(state));
      return dbi.deleteInstance(i.getInstanceId(state));
    },
    createInstance: (prettyId = generatePrettyId(i.getType()), onCreate = (dbi) => {}) => {
      const instance = state
        .set('_instance_create_time', +new Date())
        .set('_pretty_id', prettyId)
        .set('_instance_id', v4());
      const instanceId = instance.get('_instance_id');
      dbi.trackInstance(instance.get('_record_id'), instanceId);
      dbi.setInstance(instanceId, instance);
      onCreate(instanceId, entityDbInterface(dbi)(instance));
      return instanceId;
    },
    createRecord: (prettyId = generatePrettyId(i.getType())) => {
      const record = state
        .set('_record_create_time', +new Date())
        .set('_instance_id', null)
        .set('_pretty_id', prettyId)
        .set('_record_id', v4());

      dbi.trackRecord(record.get('_record_id'), record.get('_type'));
      return dbi.setRecord(record.get('_record_id'), record);
    },
    save: () => i.isInstance() ?
      dbi.setInstance(i.getInstanceId(), state) :
      dbi.setRecord(i.getRecordId(), state),
    getSameKind: () => i.isInstance() ?
      dbi.getInstance(i.getInstanceId()) :
      dbi.getRecord(i.getRecordId()),
    getRecord: (id) => id ? dbi.getRecord(id) : dbi.getRecord(i.getRecordId()),
    getInstance: (id) => id ? dbi.getInstance(id) : dbi.getInstance(i.getInstanceId()),
    findRecords: (clause) => dbi.findRecords(clause),
    instanceExists: (id) => dbi._getInstanceRaw(id) ?
      true : false,
    // debug only
    _parentDbi: () => dbi
  };

  return i;
};

const dbInitialState = Map({
  instances: Map(),
  records: Map(),
});

const createDatabase = ({ loadState, hook, extensions }) => {
  let state = dbInitialState.merge(loadState);
  let instanceTracker = trackInstances();
  let recordTracker = trackRecords();

  const i = Object.assign({}, extensions || {}, {
    getInstance: (id) => {
      const instance = state.getIn(['instances', id]);
      return getInterface(instance.get('_type'), instance, entityDbInterface(i));
    },
    findInstances: (clause) => state.get('instances')
      .map((id) => i.getInstance(id)).filter(clause),
    setInstance: (id, value) => {
      state = state.setIn(['instances', id], value.set('_instance_modify_time', +new Date()));
      hook(state.hashCode());
      return value.get('_instance_id');
    },
    deleteInstance: (id) => {
      state = state.deleteIn(['instances', id]);
      hook(state.hashCode());
      return null;
    },
    createRecord: (state) => entityDbInterface(i)(state).createRecord(),
    getRecord: (id) => {
      const record = state.getIn(['records', id]);
      return getInterface(record.get('_type'), record, entityDbInterface(i));
    },
    findRecords: (clause) => state.get('records')
      .map((id) => i.getRecord(id)).filter(clause),
    setRecord: (id, value) => {
      state = state.setIn(['records', id], value.set('_record_modify_time', +new Date()));
      hook(state.hashCode());
      return value.get('_record_id');
    },
    deleteRecord: (id) => {
      state = state.deleteIn(['records', id]);
      hook(state.hashCode());
      return null;
    },
    getAllRecordInstances: (recordId) => instanceTracker.getInstances(recordId)
      .map((id) => i.getInstance(id)),
    getAllRecordsByType: (type) => recordTracker.getRecordsByType(type)
      .map((id) => i.getRecord(id)),
    trackInstance: (recordId, instanceId) => {
      instanceTracker = instanceTracker.addInstance(recordId, instanceId);
    },
    trackRecord: (recordId, type) => {
      recordTracker = recordTracker.addRecordByType(type, recordId);
    },
    untrackInstance: (recordId, instanceId) => {
      instanceTracker = instanceTracker.removeInstance(recordId, instanceId);
    },
    untrackRecord: (type, recordId) => {
      recordTracker = recordTracker.removeInstance(type, recordId);
    },
    _getInstanceRaw: (id) => state.getIn(['instances', id]),
    _getRecordRaw: (id) => state.getIn(['records', id]),
    _value: () => state
  });

  return i;
};

module.exports = createDatabase;