const fs = require('fs');
const { getCampaignTime } = require('../');
const { isImmutable, List, Map } = require('immutable');

const _default_log = Map({
  user: List(),
  debug: List()
});

let _log = _default_log;
let _severity = 0;

module.exports = {
  buildMessage: (source, action, target, result) => {
    if (_severity === 0) {
      const sourceStr = `${source.getName()}[${source.getInstanceId()}|${source.getUserId()}]`;
      const targetStr = `${target.getName()}[${target.getInstanceId()}|${target.getUserId()}]`;
      if (isImmutable(result) && result.has('detail')) {
        result = result.get('detail');
      }
      return result ? 
        `${sourceStr} ${action} ${targetStr}` :
        `${sourceStr} ${action} ${targetStr} with result ${result}`;
    }
    return `${source.getName()} ${action} ${target.getName()} with result ${result}`;
  },
  clearLog: () => {
    _log = _defaut_log;
    return null;
  },
  log: (type, message, severity = 1) => {
    if (severity < _severity) {
      return;
    }
    const timeFormat = type === 'user' ? 'pretty' : 'debug';
    _log = _log.update(type, (lst) => lst.push(Map({
      time: getCampaignTime.getCurrentTime(timeFormat),
      message
    })));
  },
  logToFile: (file = './log.json') => {
    fs.writeFileSync(JSON.stringify(_log.toJS(), null, 3));
  },
  getLog: (type) => type ? _log.get(type) : _log,
  setSeverity: (newSeverity) => {
    _severity = newSeverity;
  }
};
