const { Set, List } = require('immutable');
const initialState = require('../interfaces/common/initial-state');
const checkCondition = require('./conditions');
const { inRangeInc } = require('./util');

function processDirectives(directives, dbi) {
  return directives.map((directive) => {
    if (directive.type === 'create') {
      return dbi()
        .getRecord(directive.record_id).dbi()
        .createInstance();
    }
    if (directive.type === 'destroy') {
      return dbi()
        .getInstance(directive.instance_id).dbi()
        .deleteInstance();
    }
  });
}

function trigger(incState = initialState('trigger'), dbi) {
  const rawState = initialState('trigger').merge(incState);
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return trigger(newState, dbi);
  }

  const t = {
    dbi: () => dbi,
    getType: () => state().get('type'),
    getDirectives: () => state().get('directives'),
    getConditions: () => state().get('conditions'),
    addDirective: (directive, index) => write(state()
      .update('directives', List(), (directives) =>
        directives.splice(index || directives.size, 0, directive)
      )),
    removeDirective: (index) => write(state()
      .update('directives', List(), (directives) =>
        directives.remove(index)
      )),
    addCondition: (condition, index) => write(state()
      .update('conditions', List(), (conditions) =>
        conditions.splice(index || conditions.size, 0, condition)
      )),
    removeCondition: (index) => write(state()
      .update('conditions', List(), (conditions) =>
      conditions.remove(index)
      )),
    testConditions: () => t.getConditions().reduce((result, condition) => {
      if (result === false) {
        return result;
      }
      return checkCondition(condition, dbi);
    }, true),
    process: () => {
      if (t.isProcessed()) {
        return null;
      }
      if (t.testConditions()) {
        return processDirectives(state().get('directives', List()), dbi);
      }
      return null;
    },
    isProcessed: () => state().get('_is_processed'),
    reset: () => write(state().set('_is_processed', false)),
    resetHard: () => write(state()
      .set('_is_processed', false)
      .set('triggers', List())
      .set('conditions', List())
    ),
    _markProcessed: () => write(state().set('_is_processed', true))
  };

  return t;
}

function event(incState = initialState('event'), dbi) {
  const rawState = initialState('event').merge(incState);
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return event(newState, dbi);
  }

  const e = {
    dbi: () => dbi,
    getName: () => state().get('name'),
    getDescription: () => state().get('description'),
    getTriggers: () => state().get('triggers')
      .map((triggerId) => dbi().getRecord(triggerId)),
    processTriggers: () => e.getTriggers()
      .forEach((trig) => trig.process()),
    setName: (name) => write(state().set('name', name)),
    setDescription: (desc) => write(state().set('description', desc)),
    addTrigger: (triggerId) => write(state().update('triggers', Set(), (triggers) => triggerId ? triggers.add(triggerId) : triggers))
  };

  return e;
}

function eventQueue(rawState = initialState('events'), dbi) {
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return eventQueue(newState, dbi);
  }

  const eq = {
    getEvents: (turn) => state().getIn(['queue', turn], List()).map((eventId) => dbi().getRecord(eventId)),
    getAllEvents: (turnRange) => turnRange ? state()
      .get('queue')
      .filter((eventId, turn) => inRangeInc(turn, ...turnRange))
      .map((eventId) => dbi().getRecord(eventId)) :
      state().get('queue').map((eventId) => dbi().getRecord(eventId)),
    createEvent: (name = null, description = null, triggerId) => {
      const recordId = event().dbi()
        .createRecord();
      const eventEntity = dbi().getRecord(recordId);

      const updatedEvent = eventEntity
        .setName(name)
        .setDescription(description);

      if (triggerId) {
        updatedEvent.addTrigger(triggerId);
      }
      return recordId;
    },
    scheduleEvent: (eventId, turnTime) => write(state().updateIn(['queue', turnTime], Set(), (s) => s.add(eventId))),
    removeEvent: (deleteEventId) => write(state()
      .filterNot('queue', (eventId) => eventId === deleteEventId)
    ),
  };
  return eq;
}

module.exports = {
  event,
  eventQueue,
  trigger
};

