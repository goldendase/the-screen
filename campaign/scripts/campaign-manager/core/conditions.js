const checkCondition = (condition, dbi) => {
  switch(condition.get('type')) {
    case 'exists': return dbi()
      .instanceExists(condition.get('instance_id'));
    default: return true;
  }
};

return checkCondition;
