const { fromJS, Map } = require('immutable');
const fs = require('fs');
const { compile } = require('./compile');

module.exports = {
  saveCampaign: (c, destination = { type: 'file', target: './test.json' }) => {
    const cdb = c._getCampaignDB().map((v) => v.value()).toJS();
    const cst = c._getCampaignState().map((v) => v.value()).toJS();

    if (destination.type === 'file') {
      fs.writeFileSync(destination.target, JSON.stringify({ _db: cdb, _state: cst }, null, 3));
    }
  },
  loadCampaign: (source = { type: 'file', target: '../test.json' }) => {
    if (source.type === 'file') {
      const { _db, _state } = require(source.target); // for testing only -- obv make this a proper read

      const cd = fromJS(_db).map((dbItem) => compile(dbItem.get('_type'), dbItem));
      const cs = fromJS(_state).map((stateItem) => compile(stateItem.get('_type'), stateItem));
      return Map({ campaign_database: cd, campaign_state: cs });
    }
  }
};
