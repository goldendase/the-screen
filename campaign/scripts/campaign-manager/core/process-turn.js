const processTurn = (campaign) => {
  const campaignTimer = campaign.getCampaignTimer();
  const eventQueue = campaign.eventQueue();
  const currentEvents = eventQueue.getEvents(campaignTimer.getCurrentTime());
  // XXX Likely need logic for intra-turn order as well
  // possibly based on event type
  currentEvents
    .map((eventId) => campaign.getInstance(eventId))
    .forEach((event) => event.processTrigger());
};

module.exports = processTurn;