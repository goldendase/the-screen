const { List, Map } = require('immutable');
const { isNumber, random } = require('lodash');

const prob = (chance) => random(1, 100) <= chance;

const randArr = (arr) => arr[random(0, arr.length-1)];

const roll = (diceStr) => {
  if (isNumber(diceStr)) {
    return Map({ total: diceStr, rolls: List() });
  }
  if (/^[0-9]+$/.test(diceStr)) {
    return Map({ total: Number(diceStr), rolls: List() });
  }
  if (/^[0-9]+in[0-9]+$/.test(diceStr)) {
    const [x, y] = diceStr.split('in').map((n) => Number(n));
    if (x > 0 && y > 0) {
      return Map({ total: prob(Math.floor(x / y * 100)), rolls: List() });
    } else {
      return Map({ total: 0, rolls: List() });
    }
  }
  const result = diceStr.split(',')
    .map((die) => die.trim())
    .reduce((dice, die) => {
      const [ x, y, count, sides ] = /(([0-9]+)d([0-9]+))/.exec(die);
      return dice.push({ count: Number(count), sides: Number(sides) });
    }, List())
    .reduce((total, { count, sides }) => {
      for (let i = 1; i <= count; i++) {
        const rollResult = random(1, sides);
        total = total
          .update('total', (v) => v + rollResult)
          .update('rolls', (r) => r.push(rollResult));
      }

      return total;
    }, Map({ total: 0, rolls: List() }));

  return result;
}

module.exports = {
  prob,
  roll,
  randArr
};
