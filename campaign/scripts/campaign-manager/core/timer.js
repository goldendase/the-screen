const getInitialState = require('../interfaces/common/initial-state');

const CAMPAIGN_UNIT_TIME_TRANSLATIONS = [
  [/turns?/, 1],
  [/hours?/, 6],
  [/days?/, 24 * 6],
  [/weeks?/, 7 * 24 * 6],
  [/months?/, 4 * 7 * 24 * 6]
];

const _YEAR = 12 * 4 * 7 * 24 * 6 * 1;
const _MONTH = 4 * 7 * 24 * 6 * 1;
const _DAY = 24 * 6 * 1;
const _HOUR = 6 * 1;
const _MINUTE = 1;

function timer(rawState = getInitialState('timer'), dbi) {
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return timer(newState, dbi);
  }

  const t = {
    getCurrentTime: (format = 'raw') => {
      const rawTime = state().get('_current_time');
      if (format === 'raw') {
        return rawTime;
      }

      const year = Math.floor(rawTime / _YEAR);
      const month = Math.floor((rawTime - year * _YEAR) / _MONTH);
      const day = Math.floor((rawTime - year * _YEAR - month * _MONTH) / _DAY);
      const hour = Math.floor((rawTime - year * _YEAR - month * _MONTH - day * _DAY) / _HOUR);
      const minute = Math.floor((rawTime - year * _YEAR - month * _MONTH - day * _DAY - hour * _HOUR) / _MINUTE);

      if (format === 'pretty') {
        return `Year ${year + 1}, Month ${month + 1}, Day ${day + 1}, Hour ${hour}, Minute ${minute * 10}`;
      }
      if (format === 'object') {
        return { year, month, day, hour, minute: minute * 10 };
      }
      throw new Error(`Unknown time format type ${format}`);
    },
    advance: (c = 1, units = 'turns') => {
      const target = CAMPAIGN_UNIT_TIME_TRANSLATIONS
        .find(([re]) => re.test(units));
      const [re, increment] = target;
      return write(state().set('_current_time', increment * c))
    },
    _setCurrentTime: (turn) => write(state.set('_current_time', turn)),
  };

  return t;
}

module.exports = timer;
