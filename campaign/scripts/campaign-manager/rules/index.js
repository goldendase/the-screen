module.exports = (contentType) => {
  switch(contentType) {
    case 'class': return (classType) => require('./classes')(classType);
    case 'race': return (race) => require('./races')(race);
    case 'item': return (itemType) =>  require('./items')(itemType);
    case 'world': return (worldType) => require('./worlds')(worldType);
    case 'site': return (siteType) => require('./sites')(siteType);
    case 'area': return (areaType) => require('./areas')(areaType);
    case 'ability': return (abilityType) => require('./abilities')(abilityType);
    case 'modifiers': return (modifierType) => require('./modifiers')(modifierType);
    default: () => {
      throw new Error(`No ruleset defined for ${contentType}`);
    }
  };
};

/**
 * Three kinds of constaints
 * Racial
 * Class
 * Situational
 * All with same interface, but different checks
 */