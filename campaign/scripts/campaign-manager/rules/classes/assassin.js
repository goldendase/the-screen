const defaults = {
  stats: {
    str: 12,
    dex: 12,
    int: 12
  },
  hd: '1d4',
  max_level: 15
};
