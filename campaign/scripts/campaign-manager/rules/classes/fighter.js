const { fromJS } = require('immutable');

module.exports = fromJS({
  stats: {},
  prime_req: [ 'str' ],
  hd: '1d8',
  max_level: null,
  specials: [
    {
      type: 'add',
      affects: ['num_attacks'],
      impl: 'fn-fighter-bonus-attacks',
      name: 'fighter class bonus attacks'
    }
  ],
  level: [
    { xp: 0, hd: '1d8' },
    { xp: 2035, hd: '1d8' },
    { xp: 4065, hd: '1d8' },
    { xp: 8125, hd: '1d8' },
    { xp: 16251, hd: '1d8' },
    { xp: 32501, hd: '1d8' },
    { xp: 65001, hd: '1d8' },
    { xp: 120001, hd: '1d8' },
    { xp: 240001, hd: '1d8' },
    { xp: 360001, hd: '2' },
    { xp: 480001, hd: '4' },
    { xp: 600001, hd: '6' },
    { xp: 720001, hd: '8' },
    { xp: 840001, hd: '10' },
    { xp: 960001, hd: '12' },
    { xp: 1080001, hd: '14' },
    { xp: 1200001, hd: '16' },
    { xp: 1320001, hd: '18' },
    { xp: 1440001, hd: '20' },
    { xp: 1560001, hd: '22' },
  ]
});
