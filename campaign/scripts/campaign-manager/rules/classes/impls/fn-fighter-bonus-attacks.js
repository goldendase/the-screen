module.exports = (char) => {
  if (char.getLevel() >= 15) {
    if (char.getLevel() >= 19) {
      return 2;
    }
    if (char.getLevel() >= 23) {
      return 3;
    }
    return 1;
  }
  return 0;
};
