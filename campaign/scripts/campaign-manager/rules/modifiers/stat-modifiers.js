const { Map, List } = require('immutable');
const { range, inRange } = require('lodash');

const checkRange = (range, value) => inRange(
  value,
  ...range.split('-').map((d) => Number(d))
);

const ranges = [
  '0-4',
  '4-6',
  '6-9',
  '9-13',
  '13-16',
  '16-18',
  '18-19',
  '19-99',
];

const thief_skills = [
  'pick-locks', 'pick-pockets', 'traps', 'move-silently',
  'hide-in-shadows', 'climb', 'hear-noise'
];

const genTable = (...entries) => Map(
  List(ranges).zip(entries)
);

const genThiefSkillTable = (...entries) => thief_skills
  .reduce((result, skill) => result
    .set(skill, genTable(...entries)),
  Map());

const genFullTable = (start, end, entries) => List(range(start, end+1)
  .map((i) => `${i === entries.first() ? 0 : i}-${i === entries.last() ? 99 : i+1}`)).zip(entries);

const modTable = Map({
  str: Map({
    'to-hit': genTable(-3, -2, -1, 0, 1, 2, 3, 3),
    'to-dmg': genTable(-3, -2, -1, 0, 1, 2, 3, 4),
    'force-door': genTable(-3, -2, -1, 0, 1, 2, 3, 3)
  }),
  dex: Map({
    ac: genTable(3, 2, 1, 0, -1, -2, -3, -4),
    'missile-attack': genTable(-3, -2, -1, 0, 1, 2, 3, 3),
    initiative: genTable(-2, -1, -1, 0, 1, 1, 2, 2)
  }).merge(
    genThiefSkillTable(-60, -30, -15, 0, 0, 5, 10, 15)
      .remove('hear-noise')
  ),
  con: Map({
    'hp-per-die': genTable(-3, -2, -1, 0, 1, 2, 3, 3),
    'save-poison': genTable(-2, -1, 0, 0, 0, 0, 0, 1),
    'survive-resurrection': genFullTable(3, 19,
        List([40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 92, 94, 96, 98, 100, 100])
      ),
    'survive-transformative-schock': genFullTable(3, 19,
      List([35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 93, 95, 97, 99, 99])
    )
  }),
  int: Map({
    'bonus-languages': genTable(0, 0, 0, 0, 1, 2, 3, 3),
    'learn-spell': genTable(20, 30, 35, 40, 50, 70, 75, 85, 90),
    'min-spells-per-level': genTable(2, 2, 2, 3, 4, 5, 6, 7, 8),
    'max-spells-per-level': genTable(3, 4, 5, 6, 7, 9, 11, Infinity, Infinity)
  }),
  wis: Map({
    'save-wands': genTable(-3, -2, -1, 0, 1, 2, 3, 4),
    'save-spells': genTable(-3, -2, -1, 0, 1, 2, 3, 4),
    'spell-failure': genFullTable(9, 13, List([20, 15, 10, 5, 0])),
    'additional-level-1-spells': genFullTable(12, 19, List([
      0, 1, 2, 2, 2, 2, 2, 3
    ])),
    'additional-level-2-spells': genFullTable(14, 19, List([
      0, 1, 2, 2, 2, 2
    ])),
    'additional-level-3-spells': genFullTable(16, 19, List([
      0, 1, 1, 1
    ])),
    'additional-level-4-spells': genFullTable(18, 19, List([
      0, 1, 1
    ])),
  }),
  cha: Map({
    'reaction': genTable(2, 1, 1, 0, -1, -1, -2, -2),
    'max-retainers': genTable(1, 2, 3, 4, 5, 6, 7, 7),
    'retainer-morale': genTable(4, 5, 6, 7, 8, 9, 10, 10)
  })
});

function modifier(stat, value, action) {
  if (action === 'default') {
    return Math.floor(value / 2) - 5;
  }

  if (modTable.hasIn([stat, action])) {
    return modTable.getIn([stat, action])
      .find((val, range) => checkRange(range, value)) || 0;
  }
  if (action === 'earn-xp') {
    if (inRange(value, 0, 6)) {
      return -10;
    } else if (inRange(value, 6, 9)) {
      return -5;
    } else if (inRange(value, 9, 13)) {
      return 0;
    } else if (inRange(value, 13, 16)) {
      return 5;
    } else if (inRange(value, 16, 99)) {
      return 10;
    }
  }
  return 0;
}

module.exports = modifier;