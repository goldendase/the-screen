const { inRange } = require('../core/util');
const { Map } = require('immutable');

const statModTable = [
  [[-Infinity, 3], -2],
  [[4, 7], -1],
  [[8, 13], 0],
  [[14, 17], 1],
  [[18, Infinity], 2]
];

const statModifier = (statValue) => statModTable.reduce((result, [range, mod]) =>
  inRange(statValue, ...range)) ? mod : result;

  module.exports = {
    statModifier
  };
  