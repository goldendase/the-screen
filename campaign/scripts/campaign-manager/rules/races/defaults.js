const { fromJS } = require('immutable');

const common_bonuses = {
  infravision: {
    name: 'infravision',
    affects: ['sight'],
    modifier: { type: 'permenent', value: 60 },
  },
  'sense-depth': {
    name: 'sense depth',
    type: 'usable',
    modifier: { type: 'roll', value: '2in6' }
  },
  'sense-direction-underground': {
    name: 'sense direction while underground',
    type: 'usable',
    modifier: { type: 'roll', value: '2in6' }
  }
};

const racial_bonuses = fromJS({
  dwarf: {
    bonuses: [{
      name: 'dwarven detection',
      affects: ['detect-trap', 'detect-false-wall', 'detect-construction', 'detect-slope'],
      modifier: { type: 'active', value: '2in6' }
    },
    common_bonuses.infravision
  ]
  },
  elf: {
    bonuses: [{
        name: 'elven detection',
        affects: ['detect-hidden-door'],
        modifier: { type: 'active', value: '2in6' },
      },
      common_bonuses.infravision
    ]
  },
  gnome: {
    bonuses: [
      {
        name: 'dwarven detection',
        affects: ['detect-dangerous-construction', 'detect-slope'],
        modifier: { type: 'active', value: '2in6' }
      },
      common_bonuses.infravision
    ]
  },
  halfling: {
    bonuses: [
      {
        name: 'halfling loyalty',
        affects: ['initiative'],
        modifier: { type: 'conditional', hard: 1 },
        conditions: ['solo', 'party.race.halfling']
      },
      {
        name: 'halfling accuracy',
        affects: ['missile-to-hit'],
        modifier: { type: 'permanent', hard: 1 }
      },
      {
        name: 'halfling size vs large',
        affects: ['ac'],
        modifier: { type: 'conditional', hard: -2 },
        conditions: ['attacker.size.ge.large']
      }
    ]
  },
  'half-elf': {
    bonuses: [
      {
        name: 'half-elf detection',
        affects: ['detect-hidden-door'],
        modifier: { type: 'active', value: '2in6' },
      },
      common_bonuses.infravision
    ]
  },
  'half-orc': {
    bonuses: [
      {
        name: 'half-orc detection',
        affects: ['detect-hidden-door'],
        modifier: { type: 'active', value: '2in6' },
      },
      common_bonuses.infravision
    ]
  }
});

const defaults = (race) => {
  const mm = (min = 3, max = 18) => { return { min, max }; };

  return fromJS({
    elf: {
      race: 'elf',
      cannot_wield: [],
      stat_req: {
        int: 9
      },
      stats: {
        str: mm(),
        dex: mm(7, 19),
        con: mm(6),
        int: mm(8),
        wis: mm(),
        cha: mm()
      },
      stat_bonuses: {
        dex: 1,
        con: -1
      },
      saves: {
        breath: 0,
        poison: 0,
        paralyze: 0,
        wands: 0,
        spells: 0
      },
      languages: ['common', 'elvish', 'gnoll', 'hobgoblin', 'orc'],
      immunities: [
        { type: 'paralysis', source: 'ghoul' }
      ],
      resistances: [],
      racial_bonuses: racial_bonuses.getIn(['elf', 'bonuses'], []),
      classes_available: {
        assassin: 10,
        cleric: 7,
        fighter: 10,
        'magic-user': 11,
        thief: 12
      },
      race_class_adjustments: {
        'pick-locks': -5,
        'pick-pockets': 5,
        'move-silently': 7,
        'hide-in-shadows': 10,
        'hear-noise': 3
      },
      height_range: { min: 5 * 12, max: 5 * 12 + 6 },
      mid_weight: 150
    },
    human: {
      race: 'human',
      cannot_wield: [],
      stat_req: {},
      stats: {
        str: mm(),
        dex: mm(),
        con: mm(),
        int: mm(),
        wis: mm(),
        cha: mm()
      },
      stat_bonuses: {},
      saves: {
        breath: 0,
        poison: 0,
        paralyze: 0,
        wands: 0,
        spells: 0
      },
      languages: ['common'],
      immunities: [],
      resistances: [],
      racial_bonuses: racial_bonuses.getIn(['human', 'bonuses'], []),
      classes_available: {
        assassin: 15,
        cleric: 15,
        druid: 15,
        fighter: 15,
        illusionist: 15,
        'magic-user': 15,
        monk: 15,
        paladin: 15,
        ranger: 15,
        thief: 15
      },
      race_class_adjustments: {},
      height_range: { min: 5 * 12 + 4, max: 6 * 12 + 3 },
      mid_weight: 160
    },
    dwarf: {
      race: 'dwarf',
      cannot_wield: ['2-handed', 'longbow'],
      stat_req: {
        con: 9
      },
      stats: {
        str: mm(8, 18),
        dex: mm(3, 17),
        con: mm(12, 19),
        int: mm(),
        wis: mm(),
        cha: mm(3, 16)
      },
      stat_bonuses: {
        con: 1,
        cha: -1
      },
      saves: {
        breath: 2,
        poison: 4,
        paralyze: 4,
        wands: 3,
        spells: 4
      },
      languages: ['common', 'dwarvish'],
      immunities: [],
      resistances: [],
      racial_bonuses: racial_bonuses.getIn(['dwarf', 'bonuses'], []),
      classes_available: {
        assassin: 9,
        cleric: 8,
        fighter: 9,
        thief: 12
      },
      race_class_adjustments: {
        'pick-locks': 7,
        'traps': 10,
        'climb': -10
      },
      height_range: { min: 4 * 12 - 3, max: 4 * 12 + 3 },
      mid_weight: 150
    },
    gnome: {
      race: 'gnome',
      cannot_wield: ['large', '2-handed'],
      stat_req: {
        dex: 8,
        con: 9
      },
      stats: {
        str: mm(6),
        dex: mm(),
        con: mm(8),
        int: mm(7),
        wis: mm(),
        cha: mm(8)
      },
      stat_bonuses: {},
      saves: {
        breath: 2,
        poison: 4,
        paralyze: 4,
        wands: 1,
        spells: 2
      },
      languages: ['common', 'gnomish', 'dwarvish', 'halfling', 'orc', 'goblin', 'kobold'],
      immunities: [
        { type: 'paralysis', source: 'ghoul' }
      ],
      resistances: [],
      racial_bonuses: racial_bonuses.getIn(['gnome', 'bonuses'], []),
      classes_available: {
        assassin: 8,
        cleric: 7,
        fighter: 6,
        illusionist: 7,
        thief: 12
      },
      race_class_adjustments: {
        'pick-locks': 5,
        'traps': 7,
        'move-silently': 5,
        'hide-in-shadows': 5,
        'climb': 15
      },
      height_range: { min: 4 * 12 - 3, max: 4 * 12 + 3 },
      mid_weight: 100
    },
    halfling: {
      race: 'halfling',
      cannot_wield: ['large', '2-handed'],
      stat_req: {
        dex: 9,
        con: 9
      },
      stats: {
        str: mm(6, 17),
        dex: mm(8, 18),
        con: mm(10, 19),
        int: mm(6),
        wis: mm(3, 17),
        cha: mm()
      },
      stat_bonuses: {
        str: -1,
        dex: 1
      },
      saves: {
        breath: 2,
        poison: 4,
        paralyze: 4,
        wands: 3,
        spells: 4
      },
      languages: ['common'],
      resistances: [],
      immunities: [],
      racial_bonuses: racial_bonuses.getIn(['halfling', 'bonuses'], []),
      classes_available: {
        fighter: 6,
        thief: 14
      },
      race_class_adjustments: {
        'pick-locks': 5,
        'pick-pockets': 5,
        'traps': 5,
        'move-silently': 10,
        'hide-in-shadows': 10,
        'climb': -15
      },
      height_range: { min: 3 * 12 - 1, max: 3 * 12 + 1 },
      mid_weight: 60
    },
    'half-elf': {
      race: 'half-elf',
      cannot_wield: [],
      stat_req: {},
      stats: {
        str: mm(),
        dex: mm(6),
        con: mm(6),
        int: mm(4),
        wis: mm(),
        cha: mm()
      },
      stat_bonuses: {},
      saves: {
        breath: 0,
        poison: 0,
        paralyze: 0,
        wands: 0,
        spells: 0
      },
      languages: ['common', 'elvish', 'gnoll', 'hobgoblin', 'orc'],
      immunities: [],
      resistances: [
        {
          type: 'paralysis',
          source: 'ghoul',
          modifier: 4
        }
      ],
      racial_bonuses: racial_bonuses.getIn(['half-elf', 'bonuses'], []),
      classes_available: {
        assassin: 11,
        cleric: 5,
        fighter: 12,
        'magic-user': 10,
        ranger: 8,
        thief: 12
      },
      race_class_adjustments: {
        'pick-pockets': 10,
        'hide-in-shadows': 5
      },
      height_range: { min: 5 * 12 + 2, max: 5 * 12 + 12 },
      mid_weight: 60
    },
    'half-orc': {
      race: 'half-orc',
      cannot_wield: [],
      stat_req: {
        con: 9
      },
      stats: {
        str: mm(6, 18),
        dex: mm(3, 17),
        con: mm(13, 19),
        int: mm(3, 17),
        wis: mm(3, 14),
        cha: mm(3, 12)
      },
      stat_bonuses: {},
      saves: {
        breath: 0,
        poison: 0,
        paralyze: 0,
        wands: 0,
        spells: 0
      },
      languages: ['common', 'orc'],
      immunities: [],
      resistances: [],
      racial_bonuses: racial_bonuses.getIn(['half-orc', 'bonuses'], []),
      classes_available: {
        assassin: 15,
        cleric: 4,
        fighter: 12,
        thief: 12
      },
      race_class_adjustments: {
        'pick-locks': 5,
        'traps': 5,
        'pick-pockets': 5,
        'climb': 5
      },
      height_range: { min: 5 * 12 + 5, max: 6 * 12 + 5 },
      mid_weight: 190
    }
  }).get(race);
};

module.exports = defaults;
