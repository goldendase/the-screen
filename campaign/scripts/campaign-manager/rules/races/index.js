const { notNullIn } = require('../core/util');
const { inRange } = require('lodash');

module.exports = (race) => {
  const getRace = () => {
    switch (race) {
      case 'human': return require('./human');
      case 'elf': return require('./elf');
      case 'dwarf': return require('./dwarf');
      case 'half-orc': return require('./half-orc');
      case 'half-elf': return require('./half-elf');
      case 'halfling': return require('./halfling');
      case 'gnome': return require('./gnome');
      default: () => {
        throw new Error(`Race ${race} has not been configured with a ruleset`)
      };
    }
  };

  const { constraints, defaults } = getRace(race);

  return {
    getDefaults: translateDefaults(defaults),
    generatorConstraints: translateGeneratorConstraints(constraints),
    actionConstraints: translateActionConstraints(constraints)
  };
};

function translateGeneratorConstraints(constraints) {
  const t = {
    statInRange: (stat, value) => inRange(value, ...constraints.getIn(['stat_range', stat])),
    statReqMet: (stats, key = 'stat_req') => 
        constraints.get(key).filterNot((value, stat) => 
          notNullIn(stats, stat) && stats.get(stat) >= value
        ).isEmpty(),
    statMinMet: (stats) => 
      t.statReqMet(stats, 'stat_min'),
    isValidClassLevel: (cls, level) => constraints.getIn(['valid_classes', cls]) >= level,
    getValidClasses: (stats) => {

    },
    getHeightRange: () => constraints.get('heigh_range'),
    getMidWeight: () => constraints.get('mid_weight'),
  };

  return t;
}


/**
 * Rules can apply to generators or to actions.
 * 
 * Generator rules set the parameters for the 
 * generated results. The results of generators
 * themselves (entities - e.g. 'character' or 'item) 
 * are constrained by the actions they may undertake.
 * 
 * An action rule can take the form of either a 
 * constraint or a modifier. Constraints prevent the 
 * thing from happening, while a modifier alters the
 * outcome (or the likelihood of the outcome occurring).
 * 
 * Any non-generator rule is applied to a 'source', 
 * 'target', and 'action'. 'action' is the action the
 * rule is meant to be applied to, 'source' is the actor
 * (performing the action) and 'target' is the thing
 * being acted upon. This always holds true. Even in
 * fringe cases. Two examples: 
 * (1) Drinking a potion - source and target are both the 
 * same, and the action is 'drink')
 * (2) A storm in the area reducing visibility - 'source'
 * is the area, 'target' is anyone present in the area,
 * action is 'reduce-visibility'
 * 
 * 'conditions' - ongoing action type that
 * is checked every time a source attempts to act on a target
 * when they are in the zone of control of the source of
 * the ongoing action. Zone of control means any child nodes
 * below the current root node. So, an item is in the zone of
 * control of an inventory, the inventory is in the zone of
 * control of the player, who is in the zone of control of the
 * tile, who is in the zone of control of the area, and so on.
 * Transitive property holds, so by extension the item is in the
 * zone of control of the area.
 * 
 
 * A thief cannot cast spells (constraint), an elf is
 * immune to paralysis from ghouls
 */

  // constraint_type = ['generator', 'action']
  // Generators -
  // set of validator functions and generation
  // parameters
  // Actions -
  // get map of action -> applicable constraints

  // model immunities and resistances as action modifiers
  