const { List, Map, Set } = require('immutable');

const mm = (min = 3, max = 18) => List([ min, max ]);

const constraints = Map({
  cannot_wield: Set(),
  stat_req: Map(),
  stat_min: Map(),
  stat_range: Map({
    str: mm(),
    dex: mm(),
    con: mm(),
    int: mm(),
    wis: mm(),
    cha: mm()
  }),
  valid_classes: Map({
    assassin: 15,
    cleric: 15,
    druid: 15,
    fighter: 15,
    illusionist: 15,
    'magic-user': 15,
    monk: 15,
    paladin: 15,
    ranger: 15,
    thief: 15
  }),
  height_range: List( 5 * 12 + 4, 6 * 12 + 3 ),
  mid_weight: 160,
  racial_modifiers: Map({
    skill: Map(),
    stat: Map(),
    saves: Map()
  })
});

const defaults = Map({
  languages: List(['common']),
  immunities: List(),
  resistances: List()
});

module.exports = { constraints, defaults };
