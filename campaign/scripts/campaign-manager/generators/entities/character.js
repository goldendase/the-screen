const { Map, Range, List } = require('immutable');
const { roll } = require('../../core/dice');
const names = require('../util/names');

const generatorInitialState = Map({
  skill_picks_left: 2,
  stat_pool: List(),
  finalized: false,
  stat_gen_type: null
});

const generateFirstName = () => names();
const generateLastName = () => names();
const genHeight = () => 99999;

const DEFAULT_STAT_POOL = List([14, 12, 11, 10, 9, 7]);

function g(rawState, dbi, entityInitialState) {
  rawState = generatorInitialState.merge(rawState);
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return g(newState, dbi, entityInitialState);
  }

  const gi = {
    hashCode: () => state().hashCode(),
    getEntityId: () => state().get('generated_entity_record_id', null),
    hasEntity: () => gi.getEntityId() !== null,
    getEntity: () => dbi(state()).getRecord(gi.getEntityId()),

    setEntityId: (id) => write(state().set('generated_entity_record_id', id)),
    createEntity: () => write(state().set('generated_entity_record_id', 
      dbi(entityInitialState).createRecord())),
    destroyEntity: () => {
      dbi(gi.getEntity()).delete();
      return write(state().set('generated_entity_record_id', null))
    },

    setFirstName: (fname = generateFirstName()) => gi.getEntity()._setFirstName(fname),
    setLastName: (lname = generateLastName()) => gi.getEntity()._setLastName(lname),
    setHeight: (h = genHeight()) => gi.getEntity()._setHeight(h),
    isFinalized: () => state().get('finalized'),
    finalize: () => write(state().set('finalized', true)),
    hasStatGenType: () => state().get('stat_gen_type', null) !== null,
    isGenTypeRoll: () => state().get('stat_gen_type') === 'roll',
    isGenTypeAssign: () => state().get('stat_gen_type') === 'assign',
    setGenTypeRoll: () => write(state().set('stat_gen_type', 'roll')).rollStatPool(),
    setGenTypeAssign: () => write(state().set('stat_gen_type', 'assign')).setDefaultStatPool().setApplied14Set(),
    hasStatPool: () => !state().get('stat_pool', List()).isEmpty(),
    getStatPool: () => state().get('stat_pool'),
    getNextUnassignedStat: () => gi.getEntity().getStats().findKey((v) => !v),
    setDefaultStatPool: () => gi.hasStatPool() ?
      null :
      write(state().set('stat_pool', DEFAULT_STAT_POOL)),
    rollStatPool: () => write(state().set('stat_pool',
      Range(0, 6).map(() => roll('3d6').get('total'))
    )),
    applyStatFromPool: (val, stat) => {
      if (gi.getStatPool().includes(val)) {
        gi.getEntity()._setStat(stat, val);
        return write(state().update('stat_pool', List(), (lst) => lst.toList().remove(val)));
      }
      return g(state());
    },
    hasApplied14Set: () => state().get('applied_14', false),
    setApplied14Set: () => write(state().set('applied_14', true)),
    apply14Set: (stat) => {
      if (gi.hasApplied14Set()) {
        return g(state(), dbi, entityInitialState);
      }
      gi.getEntity()._setStat(stat, 14);
      return gi.setApplied14Set();
    },
    setBackground: (background) => state().get('background', null) ?
      gi.getEntity()._setBackground(background) :
      g(state()),
    // XXX Should return a mapping of perk type (combat, magic, ) -> count
    getAvailableSpecialsCount: () => state().get('available_specials'),
    addSpecial: (specialId) => gi.getEntity().addSpecial(specialId),
    removeSpecial: (specialId) => gi.getEntity().removeSpecial(specialId),
  };
  return gi;
};

module.exports = g;
