const initialState = require('../interfaces/common/initial-state');

const generators = {
  character: require('./entities/character')
};

module.exports = (state, dbi) => {
  state = initialState('generator').merge(state);

  const entityInitialState = initialState(state.get('generator_type'));

  const provideDbi = (genInterface) => genInterface(state, dbi, entityInitialState);

  switch(state.get('generator_type')) {
    case 'character': return provideDbi(generators.character);

    default: () => {
      throw new Error(`No generator defined for type ${type}`);
    }
  };
}
