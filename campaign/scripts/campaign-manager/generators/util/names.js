const { species } = require('fantastical');
const { roll } = require('../../core/dice');

module.exports = (race = 'human', gender = 'male') => {
  if (race === 'half-elf') {
    if (roll('1d2').get('total') === 1) {
      race = 'elf';
    } else {
      race = 'human';
    }
  }
  if (race === 'half-orc') {
    if (roll('1d2').get('total') === 1) {
      race = 'orc';
    } else {
      race = 'human';
    }
  }
  return species[race](gender);
};
