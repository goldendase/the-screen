const { Map } = require('immutable');

const FIELDS = Map({
  _path: ['events'],
  _triggers: null,
  id: null,
  name: null,
  type: null,
  description: null  
});

module.exports = {
  create: (input, campaign) => campaign
      .insert(input, { schema: FIELDS })
};
