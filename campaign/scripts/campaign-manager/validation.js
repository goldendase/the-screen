const schemaError = require('./helpers.js');

module.exports = ({ obj, schema }, campaign) => {
  const se = (error) => schemaError(error, schema._path);
  if (!obj.has('id')) {
    return se('Missing id on input object');
  }
  const missingKeys = schema.keySeq().toSet()
    .subtract(obj.keySeq().toSet());
  if (!missingKeys.isEmpty()) {
    return se(`Missing required fields ${missingKeys.toJS()}`);
  };

  return null;
};
