const { Set } = require('set');
const initialState = require('./common/initial-state');

const gm = (rawState = initialState('gm'), dbi) => {
  const state = () => dbi(rawState).state();
  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    grantControl: (playerId, instanceId) => dbi(state()
      .updateIn(['player_control', playerId], Set(), (s) => s.add(instanceId))).save(),
    revokeControl: (playerId, instanceId) => dbi(state()
      .updateIn(['player_control', playerId], Set(), (s) => s.remove(instanceId))).save(),
    playerControlsInstance: (playerId, instanceId) => state()
      .getIn(['player_control', playerId]).has(instanceId)
  };
};

module.exports = gm;