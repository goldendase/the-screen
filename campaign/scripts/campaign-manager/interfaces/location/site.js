const initialState = require('../common/initial-state');

const site = (rawState = initialState('site'), dbi) => {
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return site(newState, dbi);
  }

  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getAreas: () => state().get('areas'),
    addConnection: (siteInstanceId) => write(state.setIn(['connections', siteInstanceId], true)),
    removeConnection: (siteInstanceId) => write(state().removeIn(['connections', siteInstanceId], true)),
    setName: (name) => write(state().set('name', name)),
    getName: () => state().get('name'),
    setDescription: (text) => write(state.set('description', text))
  };
};

module.exports = site;
