const { Map } = require('immutable');
const initialState = require('../common/initial-state');

const world = (rawState = initialState('world'), dbi) => {
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return world(newState, dbi);
  }
  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getSites: () => state().get('sites').keySeq()
      .map((coordinates, siteId) => Map({ coordinates, site: dbi(state()).getSameType(siteId) })),
    setSiteLoc: (wc, hc, siteInstanceId) => write(state()
      .setIn(['sites', siteInstanceId], Map({ wc, hc }))),
    removeSiteLoc: (siteInstanceId) => write(state()
      .removeIn(['sites', siteInstanceId])),
    setName: (name) => write(state().set('name', name)),
    getName: () => state().get('name'),
    setDimensions: (w, h) => write(state()
      .setIn(['dimensions', 'w'], w)
      .setIn(['dimensions', 'h'], h)
    ),
    getDimensions: (d) => d ? state().getIn(['dimensions', d]) :
      state().get('dimensions')
  };
};

module.exports = world;
