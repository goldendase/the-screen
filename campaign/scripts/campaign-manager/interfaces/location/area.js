const initialState = require('../common/initial-state');

const area = (rawState = initialState('area'), dbi) => {
  const state = () => dbi(rawState).state();
  const write = (newState) => {
    dbi(newState).save();
    return area(newState, dbi);
  }

  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getTiles: () => state().get('tiles').map((tile, id) => dbi(state()).getSameType(id)),
    setTileLoc: (wc, hc, tileInstanceId) => write(state()
        .setIn(['tiles', tileInstanceId], Map({ wc, hc }))
      ),
    removeTile: (tileInstanceId) => write(state()
      .removeIn(['tiles', tileInstanceId])),
    addConnection: (areaId) => write(state().setIn(['connections', areaId], true)),
    removeConnection: (areaId) => write(state().removeIn(['connections', areaId])),
    setName: (name) => write(state().set('name', name)),
    getName: () => state().get('name'),
    setDimensions: (w, h) => write(state()
      .setIn(['dimensions', 'w'], w)
      .setIn(['dimensions', 'h'], h)
    ),
    getDimensions: (d) => d ? state().getIn(['dimensions', d]) :
      state().get('dimensions'),
    setDescription: (text) => write(state().set('description', text))
  }
};

module.exports = area;