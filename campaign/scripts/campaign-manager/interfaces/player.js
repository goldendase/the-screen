const initialState = require('./common/initial-state');

const pc = (rawState = initialState('player'), dbi) => {
  const state = () => dbi(rawState).state();
  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getCharacters: () => state()
      .get('controlled_char_instances')
      .map((id) => dbi(state()).getSameType(id))
  };
};

module.exports = pc;