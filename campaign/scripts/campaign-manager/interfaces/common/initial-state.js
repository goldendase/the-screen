const { fromJS, Set, OrderedMap, Map } = require('immutable');

const commonInitialState = Map({
  links: { records: Set(), instances: Set() },
  tags: Set()
});

const initialState = (type) => fromJS({
  area: {
    name: '',
    tiles: {},
    dimensions: { h: 10, w: 10 },
    connections: {},
    description: '',
    _type: 'area',
  },
  campaign: {
    name: null,
    campaign_timer_id: null,
    campaign_events_id: null,
    _type: 'campaign'
  },
  character: {
    location: {
      tile: null,
      area: null,
      site: null,
      world: null
    },
    gender: null,
    background: null,
    specials: [],
    class: null,
    alignment: 50,
    stats: OrderedMap({
      str: null,
      dex: null,
      con: null,
      int: null,
      wis: null,
      cha: null
    }),
    inventory: [],
    wielded: [],
    worn: [],
    _type: 'character'
  },
  inventory: {
    items: Set(),
    container_type: 'bag',
    _type: 'inventory'
  },
  faction: {
    name: null,
    description: null,
    _type: 'faction'
  },
  trigger: {
    type: 'generic',
    directives: [],
    conditions: [],
    _is_processed: false
  },
  event: {
    triggers: {},
    name: '',
    description: '',
    _type: 'event'
  },
  events: {
    _type: 'events',
    queue: OrderedMap()
  },
  generator: {
    generator_mode: null,
    generated_entity_record_id: null,
    generator_type: null,
    _type: 'generator'
  },
  item: {
    name: null,
    status: null,
    condition: null,
    price: null,
    description: null,
    modifiers: {},
    _type: 'item'
  },
  site: {
    name: '',
    conditions: [],
    tiles: {},
    dimensions: { h: 10, w: 10 },
    connections: {},
    description: '',
    _type: 'site'
  },
  world: {
    name: '',
    sites: {},
    dimensions: { h: 10, w: 10 },
    _type: 'world'
  },
  timer: {
    _current_time: 0,
    _micro_increment: 'turn',
    _type: 'timer'
  },
  note: {
    color: 'blue',
    name: null,
    text: '',
    links: [],
    _type: 'note'
  },
  notification: {
    id: null,
    name: '',
    links: [],
    display_text: '',
    status: 'new'
  },
  linkCache: {
    character_place: {},
    place_character: {}
  }
}).map((v) => commonInitialState.merge(v)).get(type, Map());

module.exports = initialState;
