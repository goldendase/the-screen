const getInitialState = require('./initial-state');
const { Set } = require('immutable');

module.exports = (model, state) => {
  return {
    getTags: () => state.get('tags'),
    setTags: (newTags) => model(state.update('tags', Set(), (tags) => tags.union(newTags))).save(),
    getLinks: () => state.getIn(['links', 'records']).map((id) => n.getRecord(id))
      .concat(state.getIn(['links', 'instances']).map((id) => n.getInstance(id))),
    link: (entityType, id) => model(state.updateIn(['links', entityType], Set(),
      (s) => s.add(id))).save(),
    unlink: (entityType, id) => model(state.updateIn(['links', entityType], Set(),
      (s) => s.remove(id))).save(),
    getInitialState
  };
};
