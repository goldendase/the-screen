const initialState = require('./common/initial-state');

const cd = (charData = initialState('character'), dbi) => {
  const state = () => dbi(charData).state();
  const write = (newState) => {
    dbi(newState).save();
    return cd(newState, dbi);
  }
  const c = {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    // *** Read ***
    getTileLocation: () => dbi(state()).getSameKind(state().getIn(['location', 'tile'])), // a tile within an area
    getAreaLocation: () => dbi(state()).getSameKind(state().getIn(['location', 'area'])), // an area witin a site
    getSiteLocation: () => dbi(state()).getSameKind(state().getIn(['location', 'site'])), // a site within a world
    getWorldLocation: () => dbi(state()).getSameKind(state().getIn(['location', 'world'])),
    getEquippedItems: () => c.getWieldedItems().concat(c.getWornItems()),
    //getThac0: () => charData.getIn(['thac0'])
    getWieldedItems: () => state().get('wielded').map((itemId) => dbi(state()).getSameKind(itemId)),
    getWornItems: () => state().get('worn').map((itemId) => dbi(state()).getSameKind(itemId)),
    getInventory: () => state().get('inventory').map((item) => dbi(state()).getSameKind(item)),
    getStats: (stat) => stat ?
      state().getIn(['stats', stat]) :
      state().get('stats'),
    getFirstName: () => state().get('first_name'),
    getLastName: () => state().get('last_name'),
    getHeight: () => state().get('height'),
    getWeight: () => state().get('weight'),
    getBackground: () => state().get('background'),
    // *** Actions ***
    wieldItem: (instanceId) => write(state()
      .update('wielded', List(), (lst) => lst.push(instanceId))),
    wearItem: (instanceId) => write(state()
      .update('worn', List(), (lst) => lst.push(instanceId))),
    // *** Modify ***
    _setFirstName: (fname) => write(state().set('first_name', fname)),
    _setLastName: (lname) => write(state().set('last_name', lname)),
    _setStat: (stat, value) => write(state().setIn(['stats', stat], value)),
    _setBackground: (background) => write(state().set('background', background)),
    _setSkillLevel: (skill, level) => write(state().setIn(['skills', skill], level)),
    _addSpecial: (specialId) => write(state().update('specials', (specials) => specials.add(specialId))),
    _removeSpecial: (specialId) => write(state().update('specials', (specials) => specials.remove(specialId))),
    _setHeight: (height) => write(state().set('height', height)),
    _setWeight: (weight) => write(state().set('weight', weight)),
    _setLocation: (location) => write(state().mergeIn(['location'], location))
  };

  return c;
};

module.exports = cd;
