const getInitialState = require('./common/initial-state');
const processTurn = require('../core/process-turn');
const { event } = require('../core/events');

const campaign = (campaignState = getInitialState('campaign'), dbi) => {
  const state = () => dbi(campaignState).state();

  const c = {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    setName: (newName) => dbi(state().set('name', newName)).save(),
    getName: () => state().get('name'),
    createGenerator: (type) => dbi(getInitialState('generator').set('generator_type', type)).createRecord(),
    getActiveGenerators: () => dbi().findRecords((record) === record.getType() === 'generator'),
    getCampaignTimer: () => dbi().getRecord(state().get('_timer_id')),
    eventQueue: () => dbi().getRecord(state().get('_events_id')),
    advanceCampaignTime: () => {
      c.getCampaignTimer().advance();
      return processTurn(c);
    },



    getRecord: (id) => dbi(state()).getRecord(id),
    getInstance: (id) => dbi(state()).getInstance(id)
  };

  return c;
};

module.exports = campaign;

