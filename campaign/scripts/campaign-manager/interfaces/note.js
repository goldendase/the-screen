const initialState = require('./common/initial-state');

const note = (rawState = initialState('note'), dbi) => {
  const state = () => dbi(rawState).state();
  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getName: () => state().get('name'),
    getDescription: () => state().get('description'),
    getLinks: () => state().get('links'),
    setName: (name) => dbi(state().set('name', name)).save(),
    setDescription: (desc) => dbi(state().set('description', desc)).save(),
    addLinks: (links) => dbi(state().update('links', Set(), (s) => s.union(links))).save(),
    removeLinks: (links) => dbi(state().update('links', Set(), (s) => s.difference(links))).save(),
    setLinks: (links) => dbi(state().set('links', links)).save()
  }
};

module.exports = note;