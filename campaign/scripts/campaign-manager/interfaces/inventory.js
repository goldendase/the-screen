const initialState = require('./common/initial-state');

const inv = (rawState = initialState('inventory'), dbi) => {
  const state = () => dbi(rawState).state();
  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getItems: () => state().get('items').map((itemId) => dbi(state()).getSameType(itemId))
  };
}

module.exports = inv;