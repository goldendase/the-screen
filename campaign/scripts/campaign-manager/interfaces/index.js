const interfaces = {
  area: require('./location/area'),
  site: require('./location/site'),
  world: require('./location/world'),
  character: require('./character'),
  gm: require('./gm'),
  inventory: require('./inventory'),
  item: require('./item'),
  note: require('./note'),
  faction: require('./faction'),
  player: require('./player'),
  campaign: require('./campaign'),

  timer: require('../core/timer'),
  trigger: require('../core/events').trigger,
  event: require('../core/events').event,
  events: require('../core/events').eventQueue,
  notification: require('../core/notifications'),

  generator: require('../generators'),
};

const resolveInterface = (type) => {
  if (interfaces.hasOwnProperty(type)) {
    return interfaces[type];
  }
  return (state) => state;
};

module.exports = (type, state, dbi) => resolveInterface(type)(state, dbi);
