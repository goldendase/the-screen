const initialState = require('./common/initial-state');

const item = (rawState = initialState('item'), dbi) => {
  const state = () => dbi(rawState).state();
  return {
    dbi: () => dbi(state()),
    hashCode: () => state().hashCode(),
    getName: () => state().get('name'),
    getDescription: () => state().get('description'),
    getModifier: (type) => state().getIn(['modifiers', type]),
  };
};

module.exports = item;