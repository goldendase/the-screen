const { create: createCampaign } = require('./');

const hook = () => console.log('... update');

const c = createCampaign(hook);

const gen = c.getRecord(c.createGenerator('character'));
gen.createEntity();
gen.setFirstName();
gen.setLastName();
gen.setGenTypeRoll().rollStatPool();
const char = gen.getEntity();
console.log(`${char.getFirstName()} ${char.getLastName()}`);
console.log(`${gen.getStatPool()}`)

const campaignTimer = c.getCampaignTimer();

console.log(campaignTimer.getCurrentTime('pretty'));

c.advanceCampaignTime();

console.log(campaignTimer.getCurrentTime('pretty'));

const charInstanceId = char.dbi().createInstance();
const charInstance = c.getInstance(charInstanceId);

console.log(charInstance.getFirstName(), charInstance.getLastName());
charInstance._setFirstName('foo');

console.log(charInstance.getFirstName(), charInstance.getLastName());




// const { generateChar } = require('./generators/entities/character');
// const { getActiveCampaign, loadCampaign, findDbObjectById, findInstanceById } = require('./');
// const { saveCampaign, loadCampaign: loadCampaignP } = require('./core/persistence');

// const char = generateChar().finalize().getDbItem().saveDb();
// saveCampaign(getActiveCampaign());
// debugger
// char._setFirstName('foo').saveDb();

// const res = loadCampaignP();
// loadCampaign(res);

// console.log(findDbObjectById(char.getDbId()).getFirstName());
